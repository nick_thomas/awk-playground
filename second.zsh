# prints market place
awk '{ print $1 }' bookreviews.tsv | head

# prints customer id
awk '{ print $2 }' bookreviews.tsv | head

# breaks when we try to print the title
# awk '{ print $6 }' bookreviews.tsv | head

# changing field separator to use tabs using -F option
awk -F '\t' '{ print $6 }' bookreviews.tsv | head

# can also be used backwards
awk -F '\t' '{ print $NF "\t" $(NF-2) }' bookreviews.tsv | head

# using NR
awk -F '\t' '{ print NR " " $(NF-2) }' bookreviews.tsv | head
