# Awk Tutorial
```
https://earthly.dev/blog/awk-examples/
```
Todo : Awk BEGIN and END actions
---

**How is each line referred to in Awk?**

It is referred as record.

---

**How is each column referred?**

It is a field.

---

**How do Awk Field variables work?**

It creates a variable for each field(column) in a record(line) ($1,$2.. $NF).
$0 refers to the whole record.

---

**How to print out fields?**

```
awk '{ print $1, $2, $7 }'
```

---

**What is the default assumption of Awk with regards to fields in a record?**

They are delimited by whitespaces.

---

**What is NF?**

NF is a variable holding the *Number of Fields* in a record.

---

**What is NR?**

NR is the number of records so far.

---

**How to pattern match in Awk?**

`awk '/pass_the_term_to_be_matched/'`

---

**What are something about Awk Pattern matching which can be used with regex?**

`{print $4}` can be preceded by a regex pattern

For example

Matches anywhere in the line
```
awk '/hello/ { print "This line contains hello", $0}'
```

Match within a specific field

```
awk '$4~/hello/ { print "This field contains hello", $4}'
```

Exact Match a field

```
awk '$4 == "hello" { print "This field is hello:", $4}'
```

---

**What is printf?**

printf works like it does in the C, uses a format string and a list of values. `%s` for string value

---

**How do I add padding for my layout?**

```
%-Ns
```
where N is my desired column width

---

**How to shorten characters?**

Using substr

```
substr(str,start,end)
```
