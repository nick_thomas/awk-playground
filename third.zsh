# Pattern matching example
echo "aa
bb
cc" | awk "/bb/"

# Possible replacement for grep
echo "
aa 10
bb 20
cc 30" | awk '/bb/ {print $2}'

# Choosing a specific book
# awk -F '\t' '/Hunger Games/ {print $6, $8 }' bookreviews.tsv | head

# Show only unique and sort
# awk -F '\t' '/Hunger Games/ {print $6}' bookreviews.tsv | sort | uniq

# Specific pattern by matching product_id field
# awk -F '\t' '$4 == "0439023483" {print $6}' bookreviews.tsv | sort | uniq

# Displaying review_date, review_headline and star rating
# awk -F '\t' '$4 == "0439023483" {print $15 "\t" $13 "\t" $8}' bookreviews.tsv | head

# Using printf
# awk -F '\t' '$4 == "0439023483" {printf "%s \t %-20s \t %s \n",$15, $13, $8}' bookreviews.tsv | head

# Using substr
awk -F '\t' '$4 == "0439023483" {printf "%s \t %-20s \t %s \n",$15, substr($13,1,20), $8}' bookreviews.tsv | head
